const boards = document.querySelectorAll("iframe");
carousel(boards, 0);

// TODO: Make it slide
function carousel (boards, currentIndex) {
  const newIndex = currentIndex < boards.length - 1 ? currentIndex + 1 : 0;
  console.log(currentIndex, newIndex);

  boards.forEach((board, index) => {
    board.style.display = index === newIndex ? 'block' : 'none';
  });

  window.setTimeout(carousel.bind(this, boards, newIndex), 15000);
}
